#version 450

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

void main() {

    float valueXf = texture(texSampler, fragTexCoord).x;

    // Fetch texture values without conversions with texelFetch.
    //float valueXf = texelFetch(texSampler, ivec2(fragTexCoord.xy), 0).x;
    
    uint valueXuint = floatBitsToUint(valueXf);

    vec4 colorUnpacked = vec4(
        valueXuint & uint(0x00FF0000),  // R
        valueXuint & uint(0x0000FF00),  // G
        valueXuint & uint(0x000000FF),  // B
        valueXuint & uint(0xFF000000)); // A

    outColor.x = colorUnpacked.x;
    outColor.y = colorUnpacked.y;
    outColor.z = colorUnpacked.z;
    outColor.w = colorUnpacked.w;
}
