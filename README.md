# vulkan-float-texture-test
## Building
```bash
mkdir build
cd build
cmake ..
cmake --build .
```
## Running
```bash
./vulkan-float-texture-test
```
